package external

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"time"
)

const ageApi = "https://api.agify.io"
const nationalityApi = "https://api.nationalize.io"
const genderApi = "https://api.genderize.io"

var client = http.Client{Timeout: 5 * time.Second}

// AgeEnrichment returns probable age by name in parameter
func AgeEnrichment(name string) (int, error) {
	req, err := http.NewRequest(http.MethodGet, ageApi, nil)
	if err != nil {
		return 0, err
	}

	params := url.Values{}
	params.Add("name", name)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	var ageByName struct {
		Age int `json:"age"`
	}
	err = json.Unmarshal(body, &ageByName)
	if err != nil {
		return 0, err
	}

	return ageByName.Age, nil
}

// GenderEnrichment returns probable gender by name in parameter
func GenderEnrichment(name string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, genderApi, nil)
	if err != nil {
		return "", err
	}

	params := url.Values{}
	params.Add("name", name)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var genderByName struct {
		Gender string `json:"gender"`
	}
	err = json.Unmarshal(body, &genderByName)
	if err != nil {
		return "", err
	}

	return genderByName.Gender, nil
}

// NationalityEnrichment returns probable nationality by name in parameter
func NationalityEnrichment(name string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, nationalityApi, nil)
	if err != nil {
		return "", err
	}

	params := url.Values{}
	params.Add("name", name)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	type country struct {
		CountryID   string  `json:"country_id"`
		Probability float64 `json:"probability"`
	}

	var nationalityByName struct {
		Country []country `json:"country"`
	}

	err = json.Unmarshal(body, &nationalityByName)
	if err != nil {
		return "", err
	}

	return nationalityByName.Country[0].CountryID, nil
}
