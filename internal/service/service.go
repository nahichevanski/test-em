package service

import (
	"context"
	"em/internal/dto"
	"em/internal/person"
	"errors"
	"fmt"
	"log/slog"
)

type Storage interface {
	AllPersons(ctx context.Context) ([]person.Person, error)
	BatchOfPersons(ctx context.Context, page int) ([]person.Person, error)
	Person(ctx context.Context, id int) (person.Person, error)
	SavePerson(ctx context.Context, p dto.Person) error
	UpdatePerson(ctx context.Context, p person.Person) error
	RemovePersonFromDB(ctx context.Context, id int) error
}

type Enricher interface {
	Enriching(fn dto.FullName) (dto.Person, error)
}

type Service struct {
	enricher Enricher
	storage  Storage
	log      *slog.Logger
}

// New returns pointer on Service struct
func New(s Storage, e Enricher, log *slog.Logger) (*Service, error) {
	const oper = "service.New"

	if s == nil || e == nil {
		log.Info("failed service creating", slog.Any("storage: ", s), slog.Any("enricher", e))
		return nil, fmt.Errorf("%s: %w", oper, errors.New("storage or enricher is nil"))
	}

	return &Service{
		storage:  s,
		enricher: e,
		log:      log,
	}, nil
}

// GivePersons - method of service layer, returns slice persons with pagination.
// If page equal 0(zero) - returns all persons
func (s *Service) GivePersons(ctx context.Context, page int) ([]person.Person, error) {
	const oper = "service.GivePersons"

	if page == 0 {
		persons, err := s.storage.AllPersons(ctx)
		if err != nil {
			s.log.Error("failed getting persons: ", oper, err.Error())
			return nil, err
		}

		return persons, nil
	}

	persons, err := s.storage.BatchOfPersons(ctx, page)
	if err != nil {
		s.log.Error("failed getting persons: ", oper, err.Error())
		return nil, err
	}

	return persons, nil
}

// GivePerson - method of service layer, returns person info from db
func (s *Service) GivePerson(ctx context.Context, id int) (person.Person, error) {
	const oper = "service.GivePerson"

	p, err := s.storage.Person(ctx, id)
	if err != nil {
		s.log.Error("failed getting person: ", oper, err.Error())
		return person.Person{}, err
	}

	return p, nil
}

// CreatePerson - method of service layer, accepts fullname from request then enriches and saves to db
func (s *Service) CreatePerson(ctx context.Context, fn dto.FullName) error {
	const oper = "service.CreatePerson"

	s.log.Debug(fmt.Sprintf("%s %s %s", fn.Name, fn.Patronymic, fn.Surname))

	person, err := s.enricher.Enriching(fn)
	if err != nil {
		s.log.Error("failed enriching person: ", oper, err.Error())
		return err
	}

	s.log.Debug(fmt.Sprintf("%s %s %s %d %s %s %s", person.Name, person.Patronymic,
		person.Surname, person.Age, person.Gender, person.Nationality, oper))

	err = s.storage.SavePerson(ctx, person)
	if err != nil {
		s.log.Error("failed saving person: ", oper, err.Error())
		return err
	}
	return nil
}

// ModifyPerson - method of service layer, update person info in db. All parameters must be not empty
func (s *Service) ModifyPerson(ctx context.Context, p person.Person) error {
	const oper = "service.ModifyPerson"

	err := s.storage.UpdatePerson(ctx, p)
	if err != nil {
		s.log.Error("failed updating person: ", oper, err.Error())
		return err
	}

	return nil
}

// RemovePerson - method of service layer, deletes person from the db
func (s *Service) RemovePerson(ctx context.Context, id int) error {
	const oper = "service.RemovePerson"

	err := s.storage.RemovePersonFromDB(ctx, id)
	if err != nil {
		s.log.Error("failed deleting person", oper, err.Error())
		return err
	}

	return nil
}
