package server

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Server struct {
	router  *chi.Mux
	httpSrv *http.Server
	log     *slog.Logger
	service Service
}

func New(service Service, log *slog.Logger) (*Server, error) {
	const oper = "server.New"

	if service == nil {
		return nil, fmt.Errorf("%s, %w", oper, errors.New("service is nil"))
	}

	return &Server{
		router:  chi.NewRouter(),
		service: service,
		log:     log,
	}, nil
}

func (s *Server) Run() error {
	const oper = "server.Run"

	s.router.Use(middleware.Logger)
	s.router.Use(middleware.Recoverer)

	s.router.Route("/persons", func(r chi.Router) {
		r.Get("/", s.getPersons)
		r.Get("/{id}", s.getPersonById)
		r.Post("/", s.postPerson)
		r.Put("/", s.putPerson)
		r.Delete("/{id}", s.deletePerson)
	})

	s.httpSrv = &http.Server{
		Addr:    os.Getenv("ADDR"),
		Handler: s.router,
	}

	err := s.httpSrv.ListenAndServe()
	if err != nil {
		s.log.Error("server didn't start listen and serve in server.Run")
		return fmt.Errorf("%s: %w", oper, err)
	}
	return nil
}

func (s *Server) Stop(ctx context.Context) error {
	const oper = "server.Stop"
	err := s.httpSrv.Shutdown(ctx)
	if err != nil {
		s.log.Error("failed shutdown in server.stop")
		return fmt.Errorf("%s: %w", oper, err)
	}
	return nil
}
