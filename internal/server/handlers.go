package server

import (
	"context"
	"em/internal/dto"
	"em/internal/person"
	"em/internal/storage"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

type Service interface {
	GivePersons(ctx context.Context, page int) ([]person.Person, error)
	GivePerson(ctx context.Context, id int) (person.Person, error)
	CreatePerson(ctx context.Context, fn dto.FullName) error
	ModifyPerson(ctx context.Context, p person.Person) error
	RemovePerson(ctx context.Context, id int) error
}

func (s *Server) getPersons(rw http.ResponseWriter, req *http.Request) {
	page := req.URL.Query().Get("page")
	if page == "" {
		page = "0"
	}

	s.log.Debug("parameter", slog.String("page", page))

	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(rw, "BAD_PAGE_NUMBER_ERROR", http.StatusBadRequest)
		return
	}

	persons, err := s.service.GivePersons(req.Context(), pageNumber)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	personsBytes, err := json.Marshal(persons)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	_, err = rw.Write(personsBytes)
	if err != nil {
		http.Error(rw, "WRITING_RESPONSE_ERROR", http.StatusInternalServerError)
		return
	}

}

func (s *Server) getPersonById(rw http.ResponseWriter, req *http.Request) {
	personID, err := strconv.Atoi(chi.URLParam(req, "id"))
	if err != nil || personID <= 0 {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	person, err := s.service.GivePerson(req.Context(), personID)
	if err != nil {
		if errors.Is(err, storage.ErrPersonNotFound) {
			http.Error(rw, "PERSON_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	personBytes, err := json.Marshal(person)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	_, err = rw.Write(personBytes)
	if err != nil {
		http.Error(rw, "WRITING_RESPONSE_ERROR", http.StatusInternalServerError)
		return
	}
}

func (s *Server) postPerson(rw http.ResponseWriter, req *http.Request) {
	var fn dto.FullName
	err := json.NewDecoder(req.Body).Decode(&fn)
	if err != nil {
		http.Error(rw, "INVALID_JSON_ERROR", http.StatusInternalServerError)
		return
	}
	s.log.Debug(fmt.Sprintf("%s %s %s", fn.Name, fn.Patronymic, fn.Surname))

	if fn.Name == "" || fn.Surname == "" {
		http.Error(rw, "EMPTY_BODY_ERROR", http.StatusBadRequest)
		return
	}

	err = s.service.CreatePerson(req.Context(), fn)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
}

func (s *Server) putPerson(rw http.ResponseWriter, req *http.Request) {
	var p person.Person
	err := json.NewDecoder(req.Body).Decode(&p)
	if err != nil {
		http.Error(rw, "INVALID_JSON_ERROR", http.StatusInternalServerError)
		return
	}

	if p.ID == 0 {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	if p.Age == 0 || p.Name == "" || p.Surname == "" || p.Gender == "" || p.Nationality == "" {
		http.Error(rw, "EMPTY_BODY_ERROR", http.StatusBadRequest)
		return
	}

	s.log.Debug(fmt.Sprintf("%d %s %s %s %d %s %s", p.ID, p.Name, p.Patronymic,
		p.Surname, p.Age, p.Gender, p.Nationality))

	err = s.service.ModifyPerson(req.Context(), p)
	if err != nil {
		if errors.Is(err, storage.ErrPersonNotFound) {
			http.Error(rw, "PERSON_NOT_FOUND_ERROR", http.StatusInternalServerError)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

func (s *Server) deletePerson(rw http.ResponseWriter, req *http.Request) {
	personID, err := strconv.Atoi(chi.URLParam(req, "id"))
	if err != nil || personID <= 0 {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	err = s.service.RemovePerson(req.Context(), personID)
	if err != nil {
		if errors.Is(err, storage.ErrPersonNotFound) {
			http.Error(rw, "PERSON_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
}
