package postgres

import (
	"context"
	"em/internal/dto"
	"em/internal/person"
	"em/internal/storage"
	"errors"
	"fmt"

	"github.com/jackc/pgx"
)

// AllPersons returns from db all persons
func (s *Storage) AllPersons(ctx context.Context) ([]person.Person, error) {
	const oper = "postgres.Persons"

	query := `
		select * from persons;
	`
	rows, err := s.db.Query(ctx, query)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrPersonNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	persons := make([]person.Person, 0, 8)

	for rows.Next() {
		p := person.Person{}
		err = rows.Scan(&p.ID, &p.Age, &p.Name, &p.Surname, &p.Patronymic, &p.Gender, &p.Nationality)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		persons = append(persons, p)
	}
	return persons, nil
}

// BatchOfPersons returns maximum 10(ten) persons by number of page
func (s *Storage) BatchOfPersons(ctx context.Context, page int) ([]person.Person, error) {
	const oper = "postgres.BatchOfPersons"

	limit := page * 10
	offset := limit - 10

	query := `
		select * from persons
		limit $1
		offset $2;
	`
	rows, err := s.db.Query(ctx, query, limit, offset)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrPersonNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	persons := make([]person.Person, 0, 8)

	for rows.Next() {
		p := person.Person{}
		err = rows.Scan(&p.ID, &p.Age, &p.Name, &p.Surname, &p.Patronymic, &p.Gender, &p.Nationality)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		persons = append(persons, p)
	}
	return persons, nil
}

// Person returns data transfer object of type Person
func (s *Storage) Person(ctx context.Context, id int) (person.Person, error) {
	const oper = "postgres.Person"

	query := `
	select id, age, name, surname, patronymic, gender, nationality
	from persons
	where id = $1;
	`
	row := s.db.QueryRow(ctx, query, id)

	p := person.Person{}

	err := row.Scan(&p.ID, &p.Age, &p.Name, &p.Surname, &p.Patronymic, &p.Gender, &p.Nationality)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return person.Person{}, storage.ErrPersonNotFound
		}
		return person.Person{}, fmt.Errorf("%s: %w", oper, err)
	}
	return p, nil
}

// SavePerson save Person in database
func (s *Storage) SavePerson(ctx context.Context, p dto.Person) error {
	const oper = "postgres.SavePerson"

	query := `
		insert into persons(age, name, surname, patronymic, gender, nationality)
		values($1, $2, $3, $4, $5, $6);
	`
	rows, err := s.db.Query(ctx, query,
		p.Age, p.Name, p.Surname, p.Patronymic, p.Gender, p.Nationality)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	return nil
}

// UpdatePerson update Person, if person not found returns ErrPersonNotFound
func (s *Storage) UpdatePerson(ctx context.Context, p person.Person) error {
	const oper = "postgres.UpdatePerson"

	query := `
		update persons
		set age = $2, name = $3, surname = $4, patronymic = $5, gender = $6, nationality = $7
		where id = $1;
	`
	comTag, err := s.db.Exec(ctx, query,
		p.ID, p.Age, p.Name, p.Surname, p.Patronymic, p.Gender, p.Nationality)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	if comTag.RowsAffected() == 0 {
		return storage.ErrPersonNotFound
	}

	return nil
}

// RemovePersonFromDB remove Person, if person not exists return  ErrPersonNotFound
func (s *Storage) RemovePersonFromDB(ctx context.Context, id int) error {
	const oper = "postgres.RemovePerson"

	query := `
		delete from persons
		where id = $1;
	`
	rows, err := s.db.Query(ctx, query, id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrPersonNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	return nil
}
