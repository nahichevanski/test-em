package client

import (
	"context"
	"em/internal/dto"
	"em/pkg/external"
	"fmt"

	"golang.org/x/sync/errgroup"
)

type Enrichment struct {
}

func New() *Enrichment {
	return &Enrichment{}
}

func (e Enrichment) Enriching(fn dto.FullName) (dto.Person, error) {
	const oper = "client.Enriching"
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	g, ctx := errgroup.WithContext(ctx)

	person := dto.Person{
		Name:       fn.Name,
		Surname:    fn.Surname,
		Patronymic: fn.Patronymic,
	}

	g.Go(func() error {
		if ctx.Err() != nil {
			return ctx.Err()
		}
		age, err := external.AgeEnrichment(fn.Name)
		if err != nil {
			cancel()
			return err
		}
		person.Age = age
		return nil
	})

	g.Go(func() error {
		if ctx.Err() != nil {
			return ctx.Err()
		}
		gender, err := external.GenderEnrichment(fn.Name)
		if err != nil {
			cancel()
			return err
		}
		person.Gender = gender
		return nil
	})

	g.Go(func() error {
		if ctx.Err() != nil {
			return ctx.Err()
		}
		nationality, err := external.NationalityEnrichment(fn.Name)
		if err != nil {
			cancel()
			return err
		}
		person.Nationality = nationality
		return nil
	})

	if err := g.Wait(); err != nil {
		return dto.Person{}, fmt.Errorf("%s: %w", oper, err)
	}

	return person, nil
}

// func (e Enrichment) Enriching(fn dto.FullName) (dto.Person, error) {
// 	const oper = "client.Enriching"
// 	ctx, cancel := context.WithCancel(context.Background())
// 	g, ctx := errgroup.WithContext(ctx)

// 	var person dto.Person
// 	var ageErr, genderErr, nationalityErr error

// 	g.Go(func() error {

// 		select {
// 		case <-ctx.Done():
// 			return ctx.Err()
// 		default:
// 			person.Age, ageErr = external.AgeEnrichment(fn.Name)
// 			if ageErr != nil {
// 				cancel()
// 				return ageErr
// 			}
// 			return nil
// 		}
// 	})

// 	g.Go(func() error {

// 		select {
// 		case <-ctx.Done():
// 			return ctx.Err()
// 		default:
// 			person.Gender, genderErr = external.GenderEnrichment(fn.Name)
// 			if genderErr != nil {
// 				cancel()
// 				return genderErr
// 			}
// 			return nil
// 		}
// 	})

// 	g.Go(func() error {

// 		select {
// 		case <-ctx.Done():
// 			return ctx.Err()
// 		default:
// 			person.Nationality, nationalityErr = external.NationalityEnrichment(fn.Name)
// 			if nationalityErr != nil {
// 				cancel()
// 				return nationalityErr
// 			}
// 			return nil
// 		}
// 	})

// 	if err := g.Wait(); err != nil {
// 		return dto.Person{}, fmt.Errorf("%s: %w", oper, err)
// 	}

// 	person.Name = fn.Name
// 	person.Surname = fn.Surname

// 	return person, nil
// }
