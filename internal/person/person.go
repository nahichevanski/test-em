package person

type Person struct {
	ID          int
	Age         int
	Name        string
	Surname     string
	Patronymic  string
	Gender      string
	Nationality string
}
