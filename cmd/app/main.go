package main

import (
	"context"
	"em/internal/client"
	"em/internal/server"
	"em/internal/service"
	"em/internal/storage/postgres"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	log := slog.New(
		slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
	)

	db, err := postgres.New(log)
	if err != nil {
		log.Error("Database crashed", err)
		os.Exit(1)
	}
	log.Info("Database connected", slog.Any("db", db))

	service, err := service.New(db, client.New(), log)
	if err != nil {
		log.Error("Service don't created: ", err)
		os.Exit(1)
	}
	log.Info("Service created", slog.Any("service", service))

	server, err := server.New(service, log)
	if err != nil {
		log.Error("Server don't created: ", err)
		os.Exit(1)
	}
	log.Info("Server created", slog.Any("server", server))

	go func() {
		err = server.Run()
		if err != nil {
			log.Error("Server don't running: ", err)
		}
	}()

	log.Info("Server started", slog.Any("server", server))
	time.Sleep(200 * time.Millisecond)
	log.Debug("Server started", slog.Any("server", server))

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-sig

	err = server.Stop(context.Background())
	if err != nil {
		log.Error("Server shutting down error")
	}
	log.Info("Server shutting down")

	db.Stop()
	log.Info("Database closed")
}
