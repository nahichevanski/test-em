CREATE TABLE IF NOT EXISTS persons (
    id SERIAL PRIMARY KEY,
    age INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    patronymic VARCHAR(255),
    gender VARCHAR (255) NOT NULL,
    nationality VARCHAR (255) NOT NULL
);