run: ## Start application
run:
	go run cmd/app/main.go

migrate up: ## Migrations
migrate up:
	go run cmd/migrator/main.go